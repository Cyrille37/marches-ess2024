<?php

namespace App\Console\Commands;

use App\Models\Client;
use App\Models\Offre;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Http;
use PHPHtmlParser\Dom;

class SlurpEss2024 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ess2024:slurp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Récupère les marches sur ess2024.org.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = 'https://ess2024.org/wp-admin/admin-ajax.php';
        $posts_per_page = 10;
        $whats = [
            [
                'action' => 'ind-olympic-loadmore',
                'post_type' => 'ind_olympic_markets',
            ],
            [
                'action' => 'olympic-loadmore',
                'post_type' => 'olympic_markets',
            ]
        ];
        $query = array(
            'post_status' => 'publish',
            'post_type' => null,
            'posts_per_page' => $posts_per_page,
            /*
            'comments_per_page' => '50',
            */
            'ignore_sticky_posts' => false,
            'suppress_filters' => false,
            'cache_results' => true,
            'update_post_term_cache' => true,
            'update_post_meta_cache' => true,
            'lazy_load_term_meta' => true,
            'nopaging' => false,
            'no_found_rows' => false,
            'order' => 'DESC',
        );

        $debug_cache = env('ESS2024_DEBUG_SLURP_CACHE');

        foreach ($whats as $what) {
            $page = 0;
            do {
                echo 'action:', $what['action'] . ' page: ', $page, "\n";

                $file = '/tmp/ess2024-' . $what['post_type'] . '-' . $posts_per_page . '-' . $page;
                if ((!$debug_cache) || (!file_exists($file))) {
                    $query['post_type'] = $what['post_type'];
                    $response = Http::asForm()->post($url, [
                        'action' => $what['action'],
                        'page' => $page,
                        'query' => json_encode($query),
                    ]);
                    if ($debug_cache) {
                        echo "\t", 'to cache ', "\n";
                        file_put_contents($file, $response);
                    }
                } else {
                    echo "\t", 'from cache ', "\n";
                    $response = file_get_contents($file);
                }
                $page ++ ;

                $data = json_decode($response);
                $data = $data->data;

                if (!empty($data)) {
                    $dom = new Dom();
                    $dom->loadStr($data);
                    //$a = $dom->find('a')[0];
                    //echo $a->text; // "click here"
                    $items = $dom->find('li.list__item');

                    echo "\t", 'items count:', count($items), "\n";
                    foreach ($items as $item) {
                        /*
                        try {
                            \call_user_func([$this, 'processItem_' . $what['post_type']], $item);
                        } catch (QueryException $ex) {
                            switch ($ex->getCode()) {
                                case 23000:
                                    // SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry
                                    break;
                                default:
                                    throw $ex;
                            }
                        }*/
                        \call_user_func([$this, 'processItem_' . $what['post_type']], $item);
                    }
                }
            } while (!empty($data));
        }
    }

    protected function processItem_ind_olympic_markets($item)
    {
        $html = $item->innerHtml;
        /*
        <div class="u-font-size-xsmall" data-uw-styling-context="true">Région: Ile-de-France</div>
        */
        if (!preg_match('/<div .*>Région: (.*)<\/div>/Uu', $html, $m))
            throw new \RuntimeException('Parsing error');
        $region = $m[1];

        /*
        <div class="impact2024-post-market_text u-font-size-small" data-uw-styling-context="true">Remise des plis : 31/07/2022</div>
        */
        if (!preg_match('/<div .*>Remise des plis : (.*)<\/div>/Uu', $html, $m))
            throw new \RuntimeException('Parsing error');
        $remise = Carbon::createFromFormat('d/m/Y', $m[1]);

        /*
        <img class="thumbnail__media" src="https://ess2024.org/wp-content/uploads/2022/06/laval-agglo.png" alt="" data-uw-styling-context="true">
        */
        $img_link = $item->find('img')[0]->getAttribute('src');;
        $client = $this->process_client( $img_link );

        /*
        <h3 class="impact2024-post-market_title  u-font-size-default u-padding-bottom-xsmall" data-uw-styling-context="true">Remplacement des équipements techniques de la piscine Saint-Nicolas</h3>
        */
        $title = $item->find('h3')[0]->innerHtml;

        /*
        <a class="btn--ghost u-margin-top-small -format-xsmall" target="_blank" href="https://centraledesmarches.com/marches-publics/Aubergenville-GRAND-PARIS-SEINE-ET-OISE-MISSION-DE-MAITRISE-D-OEUVRE-POUR-DES-TRAVAUX-D-AMENAGEMENT-DU-BASSIN-D-AVIRON-DU-STADE-NAUTIQUE-DIDIER-SIMOND-SITUE-A-MANTES-LA-JOLIE/6902169" data-uw-styling-context="true">Consulter</a>
        */
        $link = $item->find('a.btn--ghost')[0]->getAttribute('href');
        $id = sha1($link);
        if (!preg_match('#^http[s]??://(.*?)/.*#', $link, $m))
            throw new \RuntimeException('Parsing error');
        $site = $m[1];

        //echo "\t", $id, "\n";
        //echo "\t", $region, ' ', $client, ' ', $remise, ' ', $site, "\n";
        //echo "\t", $title, "\n";

        $offre = Offre::firstOrCreate(
            ['id' => $id],
            ['reference' => null,
            'region' => $region,
            'remise' => $remise,
            'client' => $client->nom,
            'title' => $title,
            'site' => $site,
            'link' => $link,
            ]
        );

    }

    protected function processItem_olympic_markets($item)
    {
        $html = $item->innerHtml;
        /*
        <div class="u-font-size-xsmall" data-uw-styling-context="true">n°JOP2024-VRD-lot2</div>
        */
        if (!preg_match('/<div .*>n°(.*)<\/div>/Uu', $html, $m))
            throw new \RuntimeException('Parsing error');
        $reference = $m[1];

        /*
        <div class="impact2024-post-market_text u-font-size-small" data-uw-styling-context="true">Remise des plis : 28/07/2022</div>
        */
        if (!preg_match('/<div .*>Remise des plis : (.*)<\/div>/Uu', $html, $m))
            throw new \RuntimeException('Parsing error');
        $remise = Carbon::createFromFormat('d/m/Y', $m[1]);

        /*
        <img class="thumbnail__media" src="https://ess2024.org/wp-content/uploads/2019/12/enttree_veille_plaine_co_dev.png" alt="" data-uw-styling-context="true">
        */
        $img_link = $item->find('img')[0]->getAttribute('src');;
        $client = $this->process_client( $img_link );

        /*
        <h3 class="impact2024-post-market_title  u-font-size-default u-padding-bottom-xsmall" data-uw-styling-context="true">Consultation marchés travaux aménagements définitifs des phases 2 et 3 de la ZAC de l’Ecoquartier fluvial de L’Ile Saint-Denis - LOT1 VRD-TERRASSEMENT</h3>
        */
        $title = $item->find('h3')[0]->innerHtml;

        /*
        <a class="btn--ghost u-margin-top-small -format-xsmall" target="_blank" href="https://marches.maximilien.fr/entreprise/consultation/811360?orgAcronyme=m1w" data-uw-styling-context="true">Consulter</a>
        */
        $link = $item->find('a.btn--ghost')[0]->getAttribute('href');;
        $id = sha1($link);
        if (!preg_match('#^http[s]??://(.*?)/.*#', $link, $m))
            throw new \RuntimeException('Parsing error');
        $site = $m[1];

        //echo "\t", $id, "\n";
        //echo "\t", $reference, ' ', $client, ' ', $remise, ' ', $site, "\n";
        //echo "\t", $title, "\n";

        $offre = Offre::firstOrCreate(
            ['id' => $id],
            ['reference' => $reference,
            'region' => null,
            'remise' => $remise,
            'client' => $client->nom,
            'title' => $title,
            'site' => $site,
            'link' => $link,
            ]
        );

    }

    protected function process_client( $img_link ) : ?Client
    {
        if (!preg_match('/^.*\/[0-9]{4}\/[0-9]{2}\/(.*).(?:png|jpg)$/Uu', $img_link, $m))
            throw new \RuntimeException('Parsing error: '.var_export($img_link,true));
        $nom = $m[1];
        $img = null ;

        $client = Client::find( $nom );
        if( ! $client || (! $client->img)  )
        {
            # get the file
            try {
                $img = file_get_contents( $img_link );
            }catch( \Exception $ex ){
                echo 'Failed to get image: ', $ex->getMessage(),"\n";
            }    
        }

        if( ! $client )
        {
            # create client
            $client = Client::create([
                'nom' => $nom,
                'img_link' => $img_link,
                'img' => $img
            ]);
        }
        else if( ! $client->img )
        {
            # update client
            $client->img = $img ;
            $client->save();
        }

        return $client ;
    }
}
