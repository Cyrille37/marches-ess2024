<?php

namespace App\Console\Commands;

use App\Models\Offre;
use App\Models\Word;
use App\Models\WordOffre;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class IndexWordsEss2024 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ess2024:indexWords';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a words index from offers.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $stopWords = [ 'and', 'avec','dans','des','ext','iii','par','pour','ses','une'];

        DB::beginTransaction();

        DB::table( WordOffre::TABLENAME)->delete();
        DB::table( Word::TABLENAME)->delete();

        $allWords = [];
        foreach( Offre::all() as $offre )
        {
            
            //$words = preg_split("/[\n\r\t ]+/", $offre->title);
            $words = preg_split('/\W/u', $offre->title);
            foreach( $words as $w )
            {
                if( strlen($w) <= 2 )
                    continue ;
                if( is_numeric($w) )
                    continue ;
                $w = strtolower( $w );
                if( in_array($w,$stopWords) )
                    continue ;
                $word = Word::firstOrCreate([ 'word' => $w ]);
                $wordOffre = WordOffre::firstOrCreate([ 'word' => $w, 'offre_id'=>$offre->id ], ['count'=>0]);
                //$wordOffre->count ++ ;
                //$wordOffre->save();
                $affected = DB::table(WordOffre::TABLENAME)
                    ->where([ 'word' => $w, 'offre_id'=>$offre->id ])
                    ->update(['count' => $wordOffre->count + 1]);

                if( isset($allWords[$w]) )
                {
                    $allWords[ $w ] ++ ;
                }
                else
                {
                    $allWords[ $w ] = 1 ;
                    
                }
            }
        }
        //ksort($allWords);
        //var_export( $allWords );

        DB::commit();
    }
}
