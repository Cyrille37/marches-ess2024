# Marchés ESS2024

Aspiration des offres du site ess2024.org

```bash
./artisan ess2024:slurp
./artisan ess2024:indexWords
```

Une instance sur [ess2024.giquello.fr](https://ess2024.giquello.fr/).
