<?php

use App\Models\Offre;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( Offre::TABLENAME, function (Blueprint $table) {

            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
        
            $table->char('id', 40);
            $table->primary('id');

            $table->string('region')->nullable();
            $table->string('reference')->nullable();
            $table->string('client');
            $table->string('site');
            $table->mediumText('title');
            $table->mediumText('link');
            $table->date('remise');
            $table->timestamps();

            $table->index('region');
            $table->index('client');
            $table->index('site');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( Offre::TABLENAME );
    }
}
