<?php

use Illuminate\Support\Facades\Log;
use App\Models\Word;
use App\Models\Client;
use App\Models\Offre;

?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Marchés ESS 2024</title>

    <!--
        https://csstools.github.io/normalize.css/
        https://nicolasgallagher.com/about-normalize-css/
    -->
    <link href="/css/normalize-8.0.1.min.css" media="all" rel="stylesheet" />
    <link href="/js/tomloprodModal-1.0.2/src/tomloprodModal.min.css" media="all" rel="stylesheet" />
    <link href="/css/style.css" media="all" rel="stylesheet" />
    <style>

    </style>

</head>

<body class="antialiased">
    <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">

        @if (Route::has('login'))
        <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            @auth
            <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a>
            @else
            <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
            @endif
            @endauth
        </div>
        @endif

        @php

        $letter = null ;

        @endphp
        <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                <div class="grid grid-cols-1 md:grid-cols-1">


                    <div class="p-6 border-t border-gray-200 dark:border-gray-700">
                        <div class="flex items-center">
                            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-500">
                                <path d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"></path>
                            </svg>
                            <div class="ml-4 text-lg leading-7 font-semibold">
                                <h2 class="underline text-gray-900 dark:text-white">Keske ?</h2>
                            </div>
                        </div>
                        <div class="ml-12">
                            <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                <p>Les appels d'offre récupérés sur
                                    <a href="https://ess2024.org" target="_blank" class="underline">ess2024.org</a>
                                    sont présentés ici avec un peu de traitement pour vous en faciliter l'exploration :-).
                                </p>
                                <p>La dernière aspiration des <strong>{!! Offre::count() !!}</strong> offres
                                    a été réalisée le <strong>{!! Word::max('updated_at') !!}</strong>.
                                </p>
                            </div>
                        </div>
                    </div>


                    <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-l">
                        <div class="flex items-center">
                            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-500">
                                <path d="M3.055 11H5a2 2 0 012 2v1a2 2 0 002 2 2 2 0 012 2v2.945M8 3.935V5.5A2.5 2.5 0 0010.5 8h.5a2 2 0 012 2 2 2 0 104 0 2 2 0 012-2h1.064M15 20.488V18a2 2 0 012-2h3.064M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                            </svg>
                            <div class="ml-4 text-lg leading-7 font-semibold">
                                <h2 class="underline text-gray-900 dark:text-white">MaisPourquoiCettePage ?</h2>
                            </div>
                        </div>
                        <div class="ml-12">
                            <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                Chacun a tendance à faire dans son coin, à réinventer la roue.
                                Si ces plateformes
                                @foreach( Offre::select('site')->distinct()->get() as $site )
                                    @if( ! $loop->first)
                                    ,
                                    @endif
                                    "<em>{{ $site->site }}</em>"
                                @endforeach
                                étaient à l'état de l'art du Web, il y aura à minima un accès <cite title="Machine to machine">M2M</cite>
                                via les protocoles
                                <a href="https://fr.wikipedia.org/wiki/RSS" class="underline" target="_blank">RSS</a>,
                                <a href="https://fr.wikipedia.org/wiki/Representational_state_transfer" class="underline" target="_blank">REST</a> ou
                                <a href="https://fr.wikipedia.org/wiki/HATEOAS" class="underline" target="_blank">HATEOAS</a>
                                légalisé par la publication des données en <a href="https://fr.wikipedia.org/wiki/Donn%C3%A9es_ouvertes" class="underline" target="_blank">Open Data</a>.
                            </div>
                        </div>
                    </div>


                    <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-l">
                        <div class="flex items-center">
                            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-500">
                                <path d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253"></path>
                            </svg>
                            <div class="ml-4 text-lg leading-7 font-semibold">
                                <h2 class="underline text-gray-900 dark:text-white">Mots</h2>
                            </div>
                        </div>
                        <div class="ml-12">
                            <p class="text-sm text-gray-500">Cliquer sur un mot pour voir les offres associées.</p>
                            <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm words">
                            @foreach( Word::orderBy('word', 'asc')
                                ->with('offres:id')->get()
                                as $word )

                                @php
                                //Log::debug( 'words', ['word'=> $word] );

                                $wl = mb_substr( $word->word, 0, 1 );
                                switch( $wl ) {
                                    case 'é': $wl = 'e'; break ;
                                    case 'é': $wl = 'e'; break ;
                                    case 'œ': $wl = 'o'; break ;
                                }
                                @endphp
                                @if( !$letter || $wl != $letter )
                                    @php
                                    $letter = $wl;
                                    @endphp
                                    <h3>{!! strtoupper($letter) !!}</h3>
                                @endif
                                <span class="word-block mr-2" data-offres="{!! $word->offres->pluck('id')->implode(',') !!}">
                                    <span class="word">{!! $word->word !!}</span>
                                    <sup>{!! $word->offres->count() !!}</sup>
                                </span>
                            @endforeach
                            </div>
                        </div>
                    </div>


                    <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-l">
                        <div class="flex items-center">
                            <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8 text-gray-500">
                                <path d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z"></path>
                                <path d="M15 13a3 3 0 11-6 0 3 3 0 016 0z"></path>
                            </svg>
                            <div class="ml-4 text-lg leading-7 font-semibold">
                                <h2 class="underline text-gray-900 dark:text-white">Émetteurs</h2>
                            </div>
                        </div>
                        <div class="ml-12">
                            <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm clients">
                            @foreach( Client::orderBy('nom', 'asc')
                                ->with('offres:id')->get()
                                as $client )
                                @php
                                Log::debug( 'clients', ['client'=> $client->nom, 'offres'=>$client->offres->count() ] );
                                @endphp
                                <figure data-offres="{!! $client->offres->pluck('id')->implode(',') !!}">
                                    @if( $client->img )
                                        <img src="data:image/png;base64,{!! base64_encode($client->img) !!}" />
                                    @else
                                        <img src="{!! $client->img_link!!}" />
                                    @endif
                                    <br /><span>{{ $client->nom }}</span>
                                </figure>
                            @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <footer class="flex justify-center mt-4 sm:items-center sm:justify-between">
                <div class="text-center text-sm text-gray-500 sm:text-left">
                    <div class="flex items-center">
                        <svg fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor" class="-mt-px w-5 h-5 text-gray-400">
                            <path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path>
                        </svg>
                        <a href="https://ess2024.org" class="ml-1 underline">
                            ess2024.org
                        </a>
                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="ml-4 -mt-px w-5 h-5 text-gray-400">
                            <path d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"></path>
                        </svg>
                        <a href="https://artefacts.coop" class="ml-1 underline">
                            Artéfacts
                        </a>
                    </div>
                </div>
                <div class="ml-4 text-center text-sm text-gray-500 sm:text-right sm:ml-0">
                    Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})
                </div>
            </footer>

        </div>
    </div>

    <!--
	https://github.com/tomloprod/tomloprodModal
    -->
    <div id="offers-dialog" data-tm-title="Offres" data-tm-bgcolor="#F1F1F1" data-tm-textcolor="#606060" class="tm-modal tm-effect tm-draggable">
        <div class="tm-wrapper">
            <div class="tm-title">
                <span class="tm-XButton tm-closeButton"></span>
                <h3 class="tm-title-text"></h3>
            </div>
            <div class="tm-content">
            </div>
        </div>
    </div>
    <script src="/js/tomloprodModal-1.0.2/src/tomloprodModal.min.js"></script>

    <script>
        var offres = <?= json_encode(Offre::select(['id', 'link', 'title', 'remise'])->get()->toArray()) ?>;
        var offresIdx = {};
        for (var i in offres) {
            offresIdx[offres[i].id] = offres[i];
        }
        TomloprodModal.start({
            borderRadius: '1em',
        });
        /**
         * Click on a word to display associated offers
         */
        document.querySelectorAll('.words .word-block').forEach((el, i) => {
            el.addEventListener('click', (ev) => {
                //console.log('click',ev.target,ev.currentTarget, ev.currentTarget.getAttribute('data-offres'));
                var offresId = ev.currentTarget.getAttribute('data-offres').split(',');

                var html = '';

                html += '<ul>';
                for( var i in offresId )
                {
                    var offre = offresIdx[ offresId[i] ] ;
                    html += '<li class="mt-2">'+offre.title ;
                    html += '<br/><span class="text-sm">Avant le:</span> '+ (new Date(offre.remise)).toDateString() ;
                    html += '<br/>'+ '<strong><a href="' + offre.link+'" class="underline" target="_blank">Consulter</a></strong>' ;
                    html += '</li>' ;
                }
                html += '</ul>';

                TomloprodModal.openModal('offers-dialog', {
                    content: html,
                });



            }, false);
        });
    </script>

</body>

</html>
